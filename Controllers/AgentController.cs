﻿using FarmingFlow.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FarmingFlow.Controllers
{
    public class AgentController : Controller
    {
        private readonly ILogger<AgentController> _logger;
        private IWebHostEnvironment hostingEnv;

        public AgentController(ILogger<AgentController> logger, IWebHostEnvironment env)
        {
            this.hostingEnv = env;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// GetClientsByID Method
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public async Task<GetAgentResponse> GetClientsByID( int _id)
        {
            try
            {
                GetAgentRequest data = new GetAgentRequest();
                data.Id = _id;

                GetAgentResponse Items = new GetAgentResponse();

                using (HttpClient client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(data);
                    HttpContent _content = new StringContent(json);
                    //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
                    var response = await client.GetAsync("https://app.supervise.ly/public/api/v3/agents.info/" + _content);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        Items = JsonConvert.DeserializeObject<GetAgentResponse>(content);
                    }
                }

                return Items;
            }
            catch { throw; }
        }

        /// <summary>
        /// GetClients Method
        /// </summary>
        /// <returns></returns>
        public async Task<List<GetAgentResponse>> GetClients()
        {
            try
            {
                //GetAgentRequest data = new GetAgentRequest();

                List<GetAgentResponse> Items = new List<GetAgentResponse>();

                using (HttpClient client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(Items);
                    HttpContent _content = new StringContent(json);
                    //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
                    var response = await client.GetAsync("https://app.supervise.ly/public/api/v3/agents.list"/* + _content*/);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        Items = JsonConvert.DeserializeObject<List<GetAgentResponse>>(content);
                    }
                }

                return Items;

            }
            catch { throw; }
        }
    }
}
