﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace FarmingFlow.Models
{
    /// <summary>
    /// Create DataSet
    /// </summary>
    #region Create DataSet
    public class CreateDataSet
    {
        [JsonProperty("projectId")]
        [Required]
        public int ProjectId { get; set; }

        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("taskId")]
        public int TaskId { get; set; }

        [JsonProperty("meta")]
        public List<object> Meta { get; set; }

        //[JsonProperty("meta")]
        //public List<object> Meta { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("createdBy")]
        public int CreatedBy { get; set; }
    }
    #endregion Create DataSet

    /// <summary>
    /// Get Dataset info by ID
    /// </summary>
    #region Get Dataset info by ID
    public class GetDataset
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        [JsonProperty("workspaceId")]
        public int WorkspaceId { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("size")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string Size { get; set; }

        [JsonProperty("imagesCount")]
        public int ImagesCount { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }
    }

    public class GetDatasetRequest
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }
    }
    #endregion Get Dataset info by ID

    /// <summary>
    /// List Datasets
    /// </summary>
    #region List Datasets
    public class DatasetsListRequest
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("sort_order")]
        public string SortOrder { get; set; }

        [JsonProperty("sort_nulls_order")]
        public string SortNullsOrder { get; set; }

        [JsonProperty("filter")]
        public List<FilterDatasets> Filter { get; set; }

        [JsonProperty("projectId")]
        [Required]
        public int ProjectId { get; set; }
    }
    public class FilterDatasets
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }


    public class DatasetsListResponse
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("perPage")]
        public int PerPage { get; set; }

        [JsonProperty("pagesCount")]
        public int PagesCount { get; set; }

        [JsonProperty("entities")]
        //public Entity[] Entities { get; set; }
        public List<EntityDatasetsListResponse> Entities { get; set; }
    }

    public class EntityDatasetsListResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("size")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string Size { get; set; }

        [JsonProperty("imagesCount")]
        public int ImagesCount { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }
    }

    #endregion List Datasets


    /// <summary>
    /// List Datasets Stats
    /// </summary>
    #region List Datasets Stats
    public class DatasetsListStats
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("sort_order")]
        public string SortOrder { get; set; }

        [JsonProperty("sort_nulls_order")]
        public string SortNullsOrder { get; set; }

        [JsonProperty("filter")]
        public List<FilterDatasetsStats> Filter { get; set; }

        [JsonProperty("projectId")]
        [Required]
        public int ProjectId { get; set; }

        [JsonProperty("archived")]
        public bool Archived { get; set; }
    }

    public class FilterDatasetsStats
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }



    public class DatasetsListStatsResponse
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("perPage")]
        public int PerPage { get; set; }

        [JsonProperty("pagesCount")]
        public int PagesCount { get; set; }

        [JsonProperty("entities")]
        //public Entity[] Entities { get; set; }
        public List<EntityDatasetsListStatsResponse> Entities { get; set; }
    }

    public class EntityDatasetsListStatsResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("size")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string Size { get; set; }

        [JsonProperty("imagesCount")]
        public int ImagesCount { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }
    }

    #endregion List Datasets Stats


    /// <summary>
    /// Remove Dataset
    /// </summary>
    #region Remove Dataset
    public class RemoveDataset
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }
    }
    #endregion Remove Dataset

    /// <summary>
    /// Update Dataset
    /// </summary>
    #region Update Dataset
    public class UpdateDataset
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
    #endregion Update Dataset
}
