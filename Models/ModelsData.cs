﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace FarmingFlow.Models
{
    /// <summary>
    /// Download Model
    /// </summary>
    #region Download Model
    public class DownloadModel
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }
    }
    #endregion Download Model

    /// <summary>
    /// Get Model info by ID
    /// </summary>
    #region Get Model info by ID
    public class GetModelByIDRequest
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }
    }

    public class GetModelByIDResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        [JsonProperty("workspaceId")]
        public int WorkspaceId { get; set; }

        [JsonProperty("taskId")]
        public object TaskId { get; set; }

        [JsonProperty("config")]
        public object Config { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("onlyTrain")]
        public bool OnlyTrain { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("pluginId")]
        public int PluginId { get; set; }

        [JsonProperty("pluginVersion")]
        public string PluginVersion { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }
    }
    #endregion Get Model info by ID


    /// <summary>
    /// List Models
    /// </summary>
    #region List Models
    public class ListModels
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("sort_order")]
        public string SortOrder { get; set; }

        [JsonProperty("sort_nulls_order")]
        public string SortNullsOrder { get; set; }

        [JsonProperty("filter")]
        //public Filter[] Filter { get; set; }
        public List<FilterListModels> Filter { get; set; }

        [JsonProperty("workspaceId")]
        [Required]
        public int WorkspaceId { get; set; }
    }

    public class FilterListModels
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }


    public class ListModelsResponse
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("perPage")]
        public int PerPage { get; set; }

        [JsonProperty("pagesCount")]
        public int PagesCount { get; set; }

        [JsonProperty("entities")]
        //public EntityListModelsResponse[] Entities { get; set; }
        public List<EntityListModelsResponse> Entities { get; set; }
    }

    public class EntityListModelsResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("taskId")]
        public object TaskId { get; set; }

        [JsonProperty("config")]
        public object Config { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("onlyTrain")]
        public bool OnlyTrain { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("pluginId")]
        public int PluginId { get; set; }

        [JsonProperty("pluginVersion")]
        public string PluginVersion { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }
    }

    #endregion List Models

}
