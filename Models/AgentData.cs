﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace FarmingFlow.Models
{
    /// <summary>
    /// Create Agent
    /// </summary>
    #region Create Agent
    public class CreateAgent
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
    }
    #endregion Create Agent
    /// <summary>
    /// Get Agent By ID
    /// </summary>
    #region Get Agent By ID
    public class Train
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class CustomResponse
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class Export
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class Import
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class Pipeline
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class InferRpc
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class Inference
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class ImportAgent
    {
        public bool enabled { get; set; }
        public int concurrency { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class TypesResponse
    {
        public Train train { get; set; }
        public CustomResponse custom { get; set; }
        public Export export { get; set; }
        public Import import { get; set; }
        public Pipeline pipeline { get; set; }
        public InferRpc infer_rpc { get; set; }
        public Inference inference { get; set; }
        public ImportAgent import_agent { get; set; }
    }

    public class CapabilitiesResponse
    {
        public TypesResponse types { get; set; }
        public List<object> allowedDockerImagesRegex { get; set; }
    }

    public class PlatformResponse
    {
        public string os { get; set; }
        public string version { get; set; }
        public string architecture { get; set; }
    }

    public class CpuResponse
    {
        public string model { get; set; }
        public int cores { get; set; }
    }

    public class HostResponse
    {
        public PlatformResponse platform { get; set; }
        public CpuResponse cpu { get; set; }
        public List<string> gpu { get; set; }
    }

    public class GetAgentResponse
    {
        [Required]
        public int id { get; set; }
        public string name { get; set; }
        public string token { get; set; }
        public int userId { get; set; }
        public int teamId { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public CapabilitiesResponse capabilities { get; set; }
        public string version { get; set; }
        public string dockerImage { get; set; }
        public HostResponse host { get; set; }
    }

    public class GetAgentRequest
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }
    }
    #endregion Get Agent By ID

    /// <summary>
    /// List Agents
    /// </summary>
    #region List Agents
    //AgentListRequest
    public class AgentListRequest
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("sort_order")]
        public string SortOrder { get; set; }

        [JsonProperty("sort_nulls_order")]
        public string SortNullsOrder { get; set; }

        [JsonProperty("filter")]
        //public FilterListAgents[] Filter { get; set; }
        public List<FilterListAgents> Filter { get; set; }

        [JsonProperty("teamId")]
        [Required]
        public int TeamId { get; set; }
    }

    public class FilterListAgents
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }


    ////AgentListResponse
    public class AgentListResponse
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("perPage")]
        public int PerPage { get; set; }

        [JsonProperty("pagesCount")]
        public int PagesCount { get; set; }

        [JsonProperty("entities")]
        //public Entity[] Entities { get; set; }
        public List<Entity> Entities { get; set; }
    }

    public class Entity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("capabilities")]
        public Capabilities Capabilities { get; set; }
    }

    public class Capabilities
    {
        [JsonProperty("types")]
        public Types Types { get; set; }

        [JsonProperty("allowedDockerImagesRegex")]
        //public object[] AllowedDockerImagesRegex { get; set; }
        public List<object> AllowedDockerImagesRegex { get; set; }
    }

    public class Types
    {
        [JsonProperty("train")]
        public Custom Train { get; set; }

        [JsonProperty("custom")]
        public Custom Custom { get; set; }

        [JsonProperty("export")]
        public Custom Export { get; set; }

        [JsonProperty("import")]
        public Custom Import { get; set; }

        [JsonProperty("pipeline")]
        public Custom Pipeline { get; set; }

        [JsonProperty("infer_rpc")]
        public Custom InferRpc { get; set; }

        [JsonProperty("inference")]
        public Custom Inference { get; set; }

        [JsonProperty("import_agent")]
        public Custom ImportAgent { get; set; }
    }

    public class Custom
    {
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("concurrency")]
        public int Concurrency { get; set; }

        [JsonProperty("allowedDockerImagesRegex")]
        //public object[] AllowedDockerImagesRegex { get; set; }
        public List<object> AllowedDockerImagesRegex { get; set; }
    }

    #endregion List Agents

    /// <summary>
    /// Update Agent
    /// </summary>
    #region Update Agent
    public class UpdateAgent
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
    #endregion Update Agent
}

