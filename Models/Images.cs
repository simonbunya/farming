﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace FarmingFlow.Models
{
    /// <summary>
    /// Add Images to Dataset
    /// </summary>
    #region List Datasets
    public class AddImages
    {
        [JsonProperty("datasetId")]
        public int DatasetId { get; set; }

        [JsonProperty("images")]
        //public Image[] Images { get; set; }
        public List<Image> Images { get; set; }

        [JsonProperty("generateUniqueNames")]
        public bool GenerateUniqueNames { get; set; }
    }
    public class Image
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("meta")]
        public Meta Meta { get; set; }

        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public string UpdatedAt { get; set; }

        [JsonProperty("createdBy")]
        public int CreatedBy { get; set; }
    }

    public class Meta
    {
    }
    #endregion List Datasets

    /// <summary>
    /// Download Image
    /// </summary>
    #region Download Image
    public class DownloadImage
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }
    }
    #endregion Download Image

    /// <summary>
    /// Get Image info by ID
    /// </summary>
    #region Get Image info by ID
    public class GetImageByIDRequest
    {
        [JsonProperty("id")]
        [Required]
        public int Id { get; set; }
    }

    public class GetImageByIDResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("datasetId")]
        public int DatasetId { get; set; }

        [JsonProperty("link")]
        public object Link { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("mime")]
        public string Mime { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("labelsCount")]
        public int LabelsCount { get; set; }
    }
    #endregion Get Image info by ID

    /// <summary>
    ///List Images
    /// </summary>
    #region List Images
    public class ListImages
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("sort_order")]
        public string SortOrder { get; set; }

        [JsonProperty("sort_nulls_order")]
        public string SortNullsOrder { get; set; }

        [JsonProperty("filter")]
        public List<FilterListImages> Filter { get; set; }

        [JsonProperty("datasetId")]
        [Required]
        public int DatasetId { get; set; }
    }

    public class FilterListImages
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }


    public class ListImagesResponse
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("perPage")]
        public int PerPage { get; set; }

        [JsonProperty("pagesCount")]
        public int PagesCount { get; set; }

        [JsonProperty("entities")]
        //public EntityListImagesResponse[] Entities { get; set; }
        public List<EntityListImagesResponse> Entities { get; set; }
    }

    public class EntityListImagesResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("datasetId")]
        public int DatasetId { get; set; }

        [JsonProperty("link")]
        public object Link { get; set; }

        [JsonProperty("mime")]
        public string Mime { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("labelsCount")]
        public int LabelsCount { get; set; }
    }
    #endregion List Images


    /// <summary>
    ///List Images Stats
    /// </summary>
    #region List Images Stats
    public class ListImagesStats
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("sort")]
        public string Sort { get; set; }

        [JsonProperty("sort_order")]
        public string SortOrder { get; set; }

        [JsonProperty("sort_nulls_order")]
        public string SortNullsOrder { get; set; }

        [JsonProperty("filter")]
        //public FilterListImagesStats[] Filter { get; set; }
        public List<FilterListImagesStats> Filter { get; set; }

        [JsonProperty("datasetId")]
        [Required]
        public int DatasetId { get; set; }

        [JsonProperty("advancedFilter")]
        //public object[] AdvancedFilter { get; set; }
        public List<object> AdvancedFilter { get; set; }
    }

    public partial class FilterListImagesStats
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }


    public class ListImagesStatsResponse
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("perPage")]
        public int PerPage { get; set; }

        [JsonProperty("pagesCount")]
        public int PagesCount { get; set; }

        [JsonProperty("entities")]
        //public EntityListImagesStatsResponse[] Entities { get; set; }
        public List<EntityListImagesStatsResponse> Entities { get; set; }
    }

    public class EntityListImagesStatsResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("datasetId")]
        public int DatasetId { get; set; }

        [JsonProperty("link")]
        public object Link { get; set; }

        [JsonProperty("mime")]
        public string Mime { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("labelsCount")]
        public int LabelsCount { get; set; }
    }
    #endregion List Images Stats


    /// <summary>
    /// Upload Image
    /// </summary>
    #region Upload Image
    public class UploadImage
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }
    }
    #endregion Upload Image


    /// <summary>
    /// Upload Image to storage
    /// </summary>
    #region Upload Image to storage
    public class UploadImageStorage
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
    #endregion Upload Image to storage


    /// <summary>
    /// Upload Images
    /// </summary>
    #region Upload Images
    public class MyArray
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }
    }
 
    public class UploadImages
    {
        public List<MyArray> MyArray { get; set; }
    }
    #endregion Upload Images
}
